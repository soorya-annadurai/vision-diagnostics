// var fontSizes = [152, 130, 108, 87, 65, 43, 33, 21, 15, 9];
var fontSizes = [152, 1, 108, 87, 65, 43, 33, 21, 15, 9];
var lettersPerLine = [1, 2, 4, 5, 6, 8, 10, 10, 10, 10];
// var interval = 20;
var interval = 2000;

$(document).ready(function () {
    $("#inputGuess").hide();
    var counter = 1;
    var myAnswerString = "";
    var totalLetters = 0;
    for (var i = 0; i < lettersPerLine.length; i++) {
        totalLetters += lettersPerLine[i];
    }
    $("#myMeter").min = 0;
    $("#myMeter").max = totalLetters;
    $("#myMeter").value = 0;
    for (var i = 0; i < lettersPerLine.length; i++) {
        for (var j = 0; j < lettersPerLine[i]; j++) {
            (function () {
                var myVar = i;
                setTimeout(function () {
                    $("#random").css("fontSize", fontSizes[myVar] + "px");
                    var nextLetter = randomString(1);
                    document.getElementById("random").innerHTML = nextLetter;
                    myAnswerString += nextLetter;
                    $("#myMeter").value = counter + 1;
                }, counter++ * interval);
            })(i);
        }
    }

    setTimeout(function () {
        document.getElementById("random").innerHTML = "";
        document.getElementById("answerString").innerHTML = myAnswerString;
        $("#inputGuess").show();
    }, counter++ * interval);

    function randomString(Length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        // var possible = "CDEFHKNPRUVZ";
        for (var i = 0; i < Length; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }
});

function levenshteinDist(s, t) {
    var d = []; //2d matrix

    // Step 1
    var n = s.length;
    var m = t.length;

    if (n === 0) return m;
    if (m === 0) return n;

    //Create an array of arrays in javascript (a descending loop is quicker)
    for (var i = n; i >= 0; i--) d[i] = [];

    // Step 2
    for (var i = n; i >= 0; i--) d[i][0] = i;
    for (var j = m; j >= 0; j--) d[0][j] = j;

    // Step 3
    for (var i = 1; i <= n; i++) {
        var s_i = s.charAt(i - 1);

        // Step 4
        for (var j = 1; j <= m; j++) {

            //Check the jagged ld total so far
            if (i === j && d[i][j] > 4) return n;

            var t_j = t.charAt(j - 1);
            var cost = (s_i === t_j) ? 0 : 1; // Step 5

            //Calculate the minimum
            var mi = d[i - 1][j] + 1;
            var b = d[i][j - 1] + 1;
            var c = d[i - 1][j - 1] + cost;

            if (b < mi) mi = b;
            if (c < mi) mi = c;

            d[i][j] = mi; // Step 6

            //Damerau transposition
            if (i > 1 && j > 1 && s_i === t.charAt(j - 2) && s.charAt(i - 2) === t_j) {
                d[i][j] = Math.min(d[i][j], d[i - 2][j - 2] + cost);
            }
        }
    }
    // Step 7
    return d[n][m];
}

function verifyResults() {
    var guess = document.getElementById("guess").value;
    var answer = document.getElementById("answerString").innerHTML;

    var cumulativeLettersPerLine = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    cumulativeLettersPerLine[0] = lettersPerLine[0];
    for (var i = 1; i < lettersPerLine.length; i++)
        cumulativeLettersPerLine[i] = cumulativeLettersPerLine[i - 1] + lettersPerLine[i];

    var guessSegments = [
        guess.substring(0, cumulativeLettersPerLine[0]),
        guess.substring(cumulativeLettersPerLine[0], cumulativeLettersPerLine[1]),
        guess.substring(cumulativeLettersPerLine[1], cumulativeLettersPerLine[2]),
        guess.substring(cumulativeLettersPerLine[2], cumulativeLettersPerLine[3]),
        guess.substring(cumulativeLettersPerLine[3], cumulativeLettersPerLine[4]),
        guess.substring(cumulativeLettersPerLine[4], cumulativeLettersPerLine[5]),
        guess.substring(cumulativeLettersPerLine[5], cumulativeLettersPerLine[6]),
        guess.substring(cumulativeLettersPerLine[6], cumulativeLettersPerLine[7]),
        guess.substring(cumulativeLettersPerLine[7], cumulativeLettersPerLine[8]),
        guess.substring(cumulativeLettersPerLine[8], cumulativeLettersPerLine[9])
    ];
    var answerSegments = [
        answer.substring(0, cumulativeLettersPerLine[0]),
        answer.substring(cumulativeLettersPerLine[0], cumulativeLettersPerLine[1]),
        answer.substring(cumulativeLettersPerLine[1], cumulativeLettersPerLine[2]),
        answer.substring(cumulativeLettersPerLine[2], cumulativeLettersPerLine[3]),
        answer.substring(cumulativeLettersPerLine[3], cumulativeLettersPerLine[4]),
        answer.substring(cumulativeLettersPerLine[4], cumulativeLettersPerLine[5]),
        answer.substring(cumulativeLettersPerLine[5], cumulativeLettersPerLine[6]),
        answer.substring(cumulativeLettersPerLine[6], cumulativeLettersPerLine[7]),
        answer.substring(cumulativeLettersPerLine[7], cumulativeLettersPerLine[8]),
        answer.substring(cumulativeLettersPerLine[8], cumulativeLettersPerLine[9])
    ];
    for (var i = 0; i < 10; i++) {
        document.getElementById('err' + i).innerHTML = 'score#' + i + "..." + (guessSegments[i].length - levenshteinDist(guessSegments[i], answerSegments[i])) + "/" + guessSegments[i].length;
    }
}
